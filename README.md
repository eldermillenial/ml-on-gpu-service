# How the Docker images are built to run ML on GPU as a Service

This repository contains two examples for running ML jobs on NuNet - one for PyTorch and one for TensorFlow to run ML jobs on GPUs. Following is how the example Dockerfiles work:

## Building Stage

This is how the image is built, to be pushed to the GitLab container registry:

### 1. Using the GPU image as base

First, we choose the base image based either on TensorFlow or PyTorch.

### 2. Always building images that are up-to-date

Before building what we want, we upgrade the image to include the latest security updates and patches.

### 3. Installing ML Source Code Dependencies

Any libraries that are required for the job to run are installed.

### 4. Downloading the ML Source Code

Now we can download the actual ML program. For example, [Fashion MNIST](https://www.kaggle.com/datasets/zalando-research/fashionmnist) on TensorFlow or [CIFAR10](https://paperswithcode.com/dataset/cifar-10) on PyTorch.

## Execution Stage

This is what the container runs when the image is deployed after pulling it from the GitLab container registry:

### Defining an Entrypoint

We define an entrypoint for the container to run the ML Job when the image gets pulled from the registry and deployed on the onboarder's machine.
