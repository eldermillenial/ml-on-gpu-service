from pathlib import Path

import lightning.pytorch as pl
from lightning.pytorch.callbacks import ModelCheckpoint

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms

# Assuming that we are on a CUDA machine, this should print a CUDA device:
device_count = torch.cuda.device_count()
print(f"GPU count: {device_count}")

# Scale batch size to number of devices
size_per_device = 32  # Match Tensorflow default batch size
print(f"batch size per device: {size_per_device}")
batch_size = size_per_device * device_count
print(f"batch size: {batch_size}")

# Load datasets and preprocessing transforms
normalize = transforms.Compose(
    [transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
)

trainset = torchvision.datasets.CIFAR10(
    root="./pytorch", train=True, download=True, transform=normalize
)
trainloader = torch.utils.data.DataLoader(
    trainset,
    batch_size=batch_size,
    shuffle=True,
    num_workers=device_count,
    persistent_workers=True,
)

testset = torchvision.datasets.CIFAR10(
    root="./pytorch", train=False, download=True, transform=normalize
)
testloader = torch.utils.data.DataLoader(
    testset,
    batch_size=batch_size,
    shuffle=False,
    num_workers=device_count,
    persistent_workers=True,
)

classes = (
    "plane",
    "car",
    "bird",
    "cat",
    "deer",
    "dog",
    "frog",
    "horse",
    "ship",
    "truck",
)


# Define a convolutional neural network to take 3-channel images
class Net(pl.LightningModule):
    loss = nn.CrossEntropyLoss()

    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 5 * 5)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

    def training_step(self, batch, batch_idx):
        # training_step defines the train loop.
        x, y = batch

        z = self(x)

        loss = self.loss(z, y)

        self.log("loss/train", loss)

        return loss

    def configure_optimizers(self):
        optimizer = optim.SGD(self.parameters(), lr=0.001, momentum=0.9)
        return optimizer


if __name__ == "__main__":
    net = Net()

    epochs = 100000

    # Get the last checkpoint path
    ckpt_paths = Path("lightning_logs")
    if not ckpt_paths.exists():
        ckpt_path = None
    else:
        ckpt_paths = list(ckpt_paths.iterdir())
        ckpt_paths.sort()
        last = ckpt_paths[-1]
        ckpt_path = ckpt_paths[-1].joinpath("checkpoints/last.ckpt")

    checkpoint_callback = ModelCheckpoint(save_last=True)

    trainer: pl.Trainer = pl.Trainer(
        accelerator="gpu",
        devices=-1,  # use all devices
        max_epochs=epochs,
        callbacks=[checkpoint_callback],
        strategy="ddp_spawn",
    )
    trainer.fit(
        model=net,
        train_dataloaders=trainloader,
        ckpt_path=ckpt_path,
    )

    print("Finished Training")
