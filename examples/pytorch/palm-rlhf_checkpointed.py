# MIT License

# Copyright (c) 2022 Phil Wang

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Checkpointed by Avimanyu Bandyopadhyay(Avi) - This allows resuming from interrupted iterations

import gzip
import random
import tqdm
import numpy as np
import os
import torch
from torch.optim import Adam
from torch.nn import functional as F
from torch.utils.data import DataLoader, Dataset
from torch.optim.lr_scheduler import ReduceLROnPlateau


from palm_rlhf_pytorch import PaLM
from accelerate import Accelerator


# Assuming that we are on a CUDA machine, this should allow printing the name of a CUDA device:

current_device=torch.cuda.current_device()

# constants

NUM_BATCHES = int(1e5)
BATCH_SIZE = 4
GRADIENT_ACCUMULATE_EVERY = 4
LEARNING_RATE = 2e-4
VALIDATE_EVERY = 100
PRIME_LENGTH = 128
GENERATE_EVERY = 500
GENERATE_LENGTH = 512
SEQ_LEN = 1024
SAVE_EVERY = 10


# helpers

def cycle(loader):
    while True:
        for data in loader:
            yield data

def decode_token(token):
    return str(chr(max(32, token)))

def decode_tokens(tokens):
    return "".join(list(map(decode_token, tokens)))


# accelerator

accelerator = Accelerator()
device = accelerator.device

# instantiate palm

model = PaLM(
    num_tokens=256,
    dim=512,
    depth=8
).to(device)

# prepare enwik8 data

dataset_dir = "./data"
if not os.path.exists(dataset_dir):
    os.makedirs(dataset_dir)
    os.system ("curl -sS -o ./data/enwik8.gz https://raw.githubusercontent.com/lucidrains/PaLM-rlhf-pytorch/main/data/enwik8.gz")
    
with gzip.open("./data/enwik8.gz") as file:
    data = np.frombuffer(file.read(int(95e6)), dtype=np.uint8).copy()
    np_train, np_valid = np.split(data, [int(90e6)])
    data_train, data_val = torch.from_numpy(np_train), torch.from_numpy(np_valid)

class TextSamplerDataset(Dataset):
    def __init__(self, data, seq_len):
        super().__init__()
        self.data = data
        self.seq_len = seq_len

    def __getitem__(self, index):
        rand_start = torch.randint(0, self.data.size(0) - self.seq_len, (1,))
        full_seq = self.data[rand_start : rand_start + self.seq_len + 1].long()
        return full_seq.to(device)

    def __len__(self):
        return self.data.size(0) // self.seq_len

train_dataset = TextSamplerDataset(data_train, SEQ_LEN)
val_dataset = TextSamplerDataset(data_val, SEQ_LEN)
train_loader = cycle(DataLoader(train_dataset, batch_size=BATCH_SIZE))
val_loader = cycle(DataLoader(val_dataset, batch_size=BATCH_SIZE))

# optimizer

optim = Adam(model.parameters(), lr=LEARNING_RATE)
scheduler = ReduceLROnPlateau(optimizer=optim, mode='min', factor=0.1, patience=5, verbose=True)


model, optim, train_loader, val_loader = accelerator.prepare(
    model, optim, train_loader, val_loader
)


checkpoint_dir = "./checkpoints"
if not os.path.exists(checkpoint_dir):
    os.makedirs(checkpoint_dir)

checkpoint_path = "./checkpoints/checkpoint.pth"
if os.path.exists(checkpoint_path):
    checkpoint = torch.load(checkpoint_path)
    #model.to(device)
    model.num_tokens = 256
    model.load_state_dict(checkpoint['model_state_dict'])
    optim.load_state_dict(checkpoint['optimizer_state_dict'])
    scheduler.load_state_dict(checkpoint['scheduler_state_dict'])
    start_iteration = checkpoint['iteration']
    print('Device used:',torch.cuda.get_device_name(current_device))
    print(f'Resuming training from iteration {start_iteration}')
else:
    print('Device used:',torch.cuda.get_device_name(current_device))
    start_iteration = 0


# training

for i in tqdm.tqdm(range(start_iteration, NUM_BATCHES), mininterval=10.0, desc="training"):
    model.train()

    for _ in range(GRADIENT_ACCUMULATE_EVERY):
        loss = model(next(train_loader), return_loss = True)
        accelerator.backward(loss / GRADIENT_ACCUMULATE_EVERY)

    accelerator.print(f"training loss: {loss.item()}")
    accelerator.clip_grad_norm_(model.parameters(), 0.5)

    optim.step()
    optim.zero_grad()

    if i % VALIDATE_EVERY == 0:
        model.eval()
        with torch.no_grad():
            loss = model(next(val_loader), return_loss = True)
            accelerator.print(f"validation loss: {loss.item()}")
            scheduler.step(loss)

    if i % GENERATE_EVERY == 0:
        model.eval()
        inp = random.choice(val_dataset)[:PRIME_LENGTH]
        prime = decode_tokens(inp)
        accelerator.print(f"%s \n\n %s", (prime, "*" * 100))

        sample = model.generate(GENERATE_LENGTH, inp[None, ...])
        output_str = decode_tokens(sample[0])
        accelerator.print(output_str, "\n")
        #accelerator.print(f"generated text: {decode_tokens(model.generate(inp, GENERATE_LENGTH))}")

    if i % SAVE_EVERY == 0:
        torch.save({
            'iteration': i + 1,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optim.state_dict(),
            'scheduler_state_dict': scheduler.state_dict()
        }, f"{checkpoint_dir}/checkpoint.pth")    

# inferencing

model.eval()
conversation_history = []

while True:
    prompt = input("Please say something [or type exit and press enter to quit]: ")
    if prompt == "exit":
        break
    conversation_history.append(prompt)
    context = " ".join(conversation_history[-len(conversation_history):])  # Use the last n inputs as context
    prompt = context + " " + prompt
    prompt = torch.tensor(list(map(ord, prompt)), dtype=torch.long).to(device)
    generated = model.generate(GENERATE_LENGTH, prompt)
    response = decode_tokens(generated)
    print(response)
