import os
import sys
import pexpect

len_parms = len(sys.argv)

if (len_parms != 3):
  print('Usage: prepare_ml.py <url_link> <ml_job_dependencies>')
  print('<ml_job_dependencies> are optional dependencies and to be separated by " " to install them each. Use "" for none.')
  exit()

url_link = sys.argv[1]

ml_dependencies = " ".join(sys.argv[2:])

parts = ml_dependencies.split(" ")

prefix = "/opt/conda/lib/python3.10/site-packages/"

prefixed_deps = [prefix + word for word in parts]

prefixed_dependencies = " ".join(prefixed_deps)

#Install ML Dependencies and Download the ML Source Code
exec_line1 = "curl -sS -o " + "ml_job.py" + " " + url_link
exec_line2 = "pip install " + ml_dependencies

#Download only the ML Source Code if no dependencies are additionally required. ml_dependencies is passed an empty value in such a case.
if (ml_dependencies == ""):

    #Run the arguments with commands to download the ML code
    download_ml_file = pexpect.spawn(exec_line1)
    download_ml_file.expect(pexpect.EOF, timeout = 10)

    #Scan the Python file for vulnerabilities    
    bandit_scan1 = pexpect.spawn(f"bandit ml_job.py")
    bandit_scan1.expect(pexpect.EOF, timeout = 300)
    #print(prefixed_dependencies)
    #print(bandit_scan)

    result1 = bandit_scan1.before.decode()    

#Run the arguments with commands to download the ML code and install dependencies
else:
    
    download_ml_file = pexpect.spawn(exec_line1)
    download_ml_file.expect(pexpect.EOF, timeout = 10)
    
    install_dependencies = pexpect.spawn(exec_line2)
    install_dependencies.expect(pexpect.EOF, timeout = 300)
    
    bandit_scan1 = pexpect.spawn(f"bandit ml_job.py")

    #Scan the Python file and dependencies for vulnerabilities
    bandit_scan2 = pexpect.spawn(f"bandit -r " + prefixed_dependencies)
    
    bandit_scan1.expect(pexpect.EOF, timeout = 300)
    bandit_scan2.expect(pexpect.EOF, timeout = 300)

    result1 = bandit_scan1.before.decode()
    result2 = bandit_scan2.before.decode()

#noissues = "No issues identified."

#low_issues = "Severity: Low"
#medium_issues = "Severity: Medium"
high_issues = "Severity: High"


# Check if the scan results of the downloaded ML source code contains any high severity issues
if (ml_dependencies == ""):
    if high_issues in result1:
        #If there are high severity issues, do not use the file: Report scan results and exit after 5 minutes
        print("----------------------------------------------------------------------------------------------------")
        print("The requested ML job appears to have critical security issues and should be reviewed & fixed")
        print("----------------------------------------------------------------------------------------------------")
        #print(result)
        #os.system('bandit ml_job.py')
        print("Scan result of the ML job:")
        print(result1)
        print("Removing the downloaded ML file...")
        os.system("rm ml_job.py")
        print("Exiting...")
        exit()
    # If there are no severity issues, use the file
    else:
        #Run the ML job
        os.system('python ml_job.py')

# Check if the scan results of the downloaded ML source code or dependencies contain any high severity issues
elif high_issues in result1 or high_issues in result2:
    # If there are severity issues, do not use the file: Report scan results and exit after 5 minutes
    print("--------------------------------------------------------------------------------------------")
    print("The requested ML job appears to have critical security issues and should be reviewed & fixed")
    print("--------------------------------------------------------------------------------------------")
    print("Scan result of the ML job:")
    print(result1)
    print("Scan result of the ML dependencies:")
    print(result2)
    print("Removing the downloaded ML file/dependencies...")
    os.system("rm ml_job.py")
    os.system("pip uninstall " + ml_dependencies)
    print("Exiting...")
    exit()
# If there are no severity issues, use the file
else:
    #Run the ML job
    os.system('python ml_job.py')
